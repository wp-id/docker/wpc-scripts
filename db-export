#!/bin/sh

set -e

if [ -z "${1+x}" ]; then
  echo "Please specify the output file."
  exit 1;
fi

OUTPUT_PATH="${1}"

case $OUTPUT_PATH in
  /*) VOLUME_DIR="$( dirname $OUTPUT_PATH )" ;;
  *) VOLUME_DIR="$( dirname "$( pwd )/${OUTPUT_PATH}" )" ;;
esac

if [ ! -d $VOLUME_DIR ]; then
  mkdir -p $VOLUME_DIR
fi

if [ $? -gt 0 ]; then
  echo "Unable to create output directory."
  exit 1
fi

DUMP_FILE="$( basename $OUTPUT_PATH )"

docker-compose run \
  --entrypoint="" \
  --name wpc-db-dump \
  --no-deps \
  --rm \
  -u "$(id -u):$(id -g)" \
  -v "${VOLUME_DIR}:/backup" \
  backend wp db export "/backup/${DUMP_FILE}"

exit $?
